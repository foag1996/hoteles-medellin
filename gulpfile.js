'use strict'

  var gulp = require('gulp'),
      sass = require('gulp-sass'),
      //require del borwser//
      browserSync = require('browser-sync'),
      //se definen las variables//
      del = require('del'),
      imagemin = require('gulp-imagemin'),
      uglify = require('gulp-uglify'),
      usemin= require('gulp-usemin'),
      rev = require('gulp-rev'),
      cleancss = require('gulp-clean-css'),
      flatmap = require('gulp-flatmap'),
      htmlmin = require('gulp-htmlmin');

   //tarea1//
   gulp.task('sass', function () {
      return gulp.src('./css/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'));
   });
   
   //tarea deñ browser//

   gulp.task('sass:watch', function() {
     gulp.watch('./css/*.scss', gulp.series('sass'));
   });

   gulp.task('browser-sync', function () {
     var files = ['./*.html', './css/*.css', './img/*.{png,jpg,gif,jpeg}', './js/*.js'];
     browserSync.init(files, {
        server: {
          baseDir: "./"
        }
     });
   });

   //tarea defaul de gulp//
   gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));

  ///tarea de borrado//
   gulp.task('clean', function(){
   	return del(['dist']);
   });

   //tarea de copyfonys-open-iconic fonts//
   gulp.task('copyfonts', function(){
   	return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
   	.pipe(gulp.dest('./dist/fonts'));
   });



   //tarea para comprimir imagenes//
   gulp.task('imagemin', function(){
   	return gulp.src('./img/*.{png,jpg,jpeg,gif}')
   	.pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
   	.pipe(gulp.dest('dist/img'));
   });

   //tarea usemin//
   gulp.task('usemin', function(){
   	return gulp.src('*.html')
   	.pipe(flatmap(function(stream, file){
   		return stream
   		.pipe(usemin({
   			css: [rev()],
   			html: [function() {return htmlmin({collapseWhitespace: true})}],
   			js: [uglify(), rev()],
   			inlinejs: [uglify()],
   			inlinecss: [cleancss(), 'concat']
   		}));
   	}))
   	.pipe(gulp.dest('dist/'));
   });

   //tarea del build//
   gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin'));