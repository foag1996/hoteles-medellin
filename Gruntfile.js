module.exports= function (grunt){
	//compilararchivos sass a css//
	grunt.initConfig({
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css',
					ext: '.css'
				}]
			}
		}
		//watch//
		watch: {
			files: ['css/*.scss'],
			task: ['css']
		}
	});
	//tareas de grunt//
	 grunt.loadNpmTasks('grunt-contrib-watch');
	 grunt.loadNpmTasks('grunt-contrib-sass');
	 grunt.registerTask('css', ['sass']);
};