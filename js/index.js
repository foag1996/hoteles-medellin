$(function () {
        //script tolltip
         $('[data-toggle="tooltip"]').tooltip();
         //script popover
         $('[data-toggle="popover"]').popover();
         //scrip tiempo de transicion de las imagenes
         $('.carousel').carousel({ 
          interval:3000
         });
         //elemento jquery
         $('#reservar').on('show.bs.modal', function (e){
            console.log('el modal reserva se esta mostrando');

            //cod para cambiar de color el boton 
            $('#reservarBtn').removeClass('btn-outline-primary');
            $('#reservarBtn').addClass('btn-primary');
            //cod para bloquear el boton 
            $('#reservarBtn').prop('disabled', true);

         });
         $('#reservar').on('shown.bs.modal', function (e){
            console.log('el modal reserva se mostró');
         });
         $('#reservar').on('hide.bs.modal', function (e){
            console.log('el modal reserva se oculta');
         });
         $('#reservar').on('hidden.bs.modal', function (e){
            console.log('el modal reserva se ocultó');
            //cod para desbloquear el boton
            $('#reservarBtn').prop('disabled', false); 
         });
       });